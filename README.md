# jeudi 20/04/2017

- problème d'instabilité de docker ? on a galéré pour construire l'image Elm qui fonctionnait parfaitement "on my machine", il a fallu redémarrer le démon docker
- ligne de commande pour un elm-live:

        $ docker run -it -v $(pwd):/spino -w spino -e LOCAL_USER_ID=$UID spino/dev elm-live --host 0.0.0.0 --source src/Spino.elm --output index.html

- on n'a pas réussi à faire la boucle complète de déploiement avec `cordova build browser` & co. Le clean/build ne semble pas fonctionner correctement, peut-être faut-il envisager d'écrire nos propres scripts (déja hackés...)
- on a un squelette fonctionnel d'application Elm qui gère une problèmatique, la définition et l'antonyme
- design des states à revoir? 
- ça manque de tests

# vendredi 14/04/2017

## Utilisation de la CLI

- missing npm -> install it on machine

> npm install -g cordova 

- download SDK CLI Tools (not studio...): https://developer.android.com/studio/index.html#downloads
- set ANDROID_HOME to where tools are downloaded and unpacked
- set PATH to tools and tools/bin directory, we need sdkmanager and android on the PATH

> export PATH=/Users/arnaud/android/tools/:$PATH
> export PATH=/Users/arnaud/android/build-tools/:$PATH
> export PATH=/Users/arnaud/android/tools/bin:$PATH
> export PATH=/Users/arnaud/android/platform-tools/:$PATH

- we need additional stuff to be installed
   - the platform(s): `sdkmanager "platforms;android-25"`
   - the build-tools: `sdkmanager "build-tools;25.0.2"`
   - system images are need if we want to run the emulator locally (runs on qmeu)
   - we should have something like the following

```
$ sdkmanager --list 
Installed packages:
  Path                              | Version | Description                       | Location                         
  -------                           | ------- | -------                           | -------                          
  build-tools;25.0.2                | 25.0.2  | Android SDK Build-Tools 25.0.2    | build-tools/25.0.2/              
  platform-tools                    | 25.0.4  | Android SDK Platform-Tools        | platform-tools/                  
  platforms;android-25              | 3       | Android SDK Platform 25           | platforms/android-25/            
  system-images;a...google_apis;x86 | 4       | Google APIs Intel x86 Atom Sys... | system-images/a...oogle_apis/x86/
  system-images;a...gle_apis;x86_64 | 4       | Google APIs Intel x86 Atom_64 ... | system-images/a...le_apis/x86_64/
  tools                             | 25.2.3  | Android SDK Tools 25.2.3          | tools/                           

...
```

  - when one wants to install packages, names are listed truncated (WTF???!!!), see [here](http://stackoverflow.com/questions/42460205/truncated-android-sdk-package-paths-from-sdkmanager-cli) for a workaround
  - then create a new avd (Android Virtual Device ?) -> easier to use UI `android avd` but there is an `android create avd`  command that allows us to build it on command-line

```
android create avd -t 1 -n "Nexus-5-AVD" -d 8 -b "google_apis/x86"
cordova run android --target=Nexus-5-AVD
``` 

 - to build and deploy to devices, one need to active ADB on device (can connect through USB or network...), then

```
cordova build android
adb install ./platforms/android/build/outputs/apk/android-debug.apk
adb uninstall io.cordova.hellocordova
```

# jeudi 13/04/2017

## Le plan:

Ca commence à 14h00 et ca se termine à 16h30

* parler vision (CEO?): 10min
* walking skeleton cordova
  * installer cordova -> check
  * get started -> ok
  * build pas sur android, on est coince dans une boucle etrange a essayer
    d'installer des trucs androidesques...
  * phonegap -> tester sur mobile sans deployer dans le store...
  * problème à résoudre: comment on code en remote sur une appli cordova?
  * on va pouvoir avoir un hello world sur nos portables (windows phone,
    android, ios...)

Ce que nous avons appris aujourd'hui:

* android sur un serveur ubuntu headless, c'est galere
* en revanche:
  * sur un windows avec android studio déjà installé/configuré: ca marche
  direct.
  => confirme l'hypothèse qu'on saurait faire une application web déployée sur 
  mobile
* sur un mac, en commande ligne, ca galère également (mais un petit peu moins...
  mais au final, marche toujours pas non plus...)
  => Installer/configurer Android Studio?
* ca marche sur un serveur en browser en hackant le run


D'ici la prochaine fois:

* mettre un watch
* Un début de backlog?
* Des idées de cinématiques écrans?
* Une première version de texte qui décrit ce que nous cherchons à faire?
* tester l'embarquement d'une appli elm dans cordova

Prochaine étape:


# mercredi 12/04/2017

Prochaine réunion: le jeudi 13/04/2017 à 14h00

Les devoirs pour la maison d'ici là:

* Un début de backlog?
* Des idées de cinématiques écrans?
* Un "client" potentiel? => Simone
* Une première version de texte qui décrit ce que nous cherchons à faire?
