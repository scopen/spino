module Spino exposing (main)

{-| Comment
@docs main
-}

import Html exposing (text, div, h1, button, textarea)
import Html.Attributes exposing (placeholder, rows, value)
import Html.Events exposing (onClick, onInput)
import SpinoCss exposing (..)

{-| Main


 -}
main : Program Never Model Msg
main =
    Html.program
        { init = Model Initial "L'Agilité" ! []
        , view = view
        , update = update
        , subscriptions = always Sub.none
        }



{- MODEL -}


type alias Problematique =
    String


type Etape
    = Initial
    | Definition { definition : String }
    | Antonyme { definition : String, antonyme : String }
    | Exemple { definition : String, antonyme : String, exemple : String }
    | These { definition : String, antonyme : String, exemple : String, these : String }
    | Terminal { definition : String, antonyme : String, exemple : String, these : String }


type alias Model =
    { etape : Etape
    , problematique : Problematique
    }



{- MESSAGES -}


type Msg
    = Suivant
    | Defini String



{- UPDATE -}


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Defini input ->
            { model | etape = defini input model.etape } ! []

        Suivant ->
            suivant model ! []


suivant : Model -> Model
suivant model =
    case model.etape of
        Initial ->
            demarrer model

        Definition { definition } ->
            antonyme definition model

        Antonyme antonyme ->
            exemple antonyme model

        Exemple exemple ->
            these exemple model

        These these ->
            { model | etape = Terminal these }

        Terminal _ ->
            model


defini : String -> Etape -> Etape
defini input etape =
    case etape of
        Initial ->
            etape

        Definition _ ->
            Definition { definition = input }

        Antonyme anto ->
            Antonyme { anto | antonyme = input }

        Exemple exemple ->
            Exemple { exemple | exemple = input }

        These these ->
            These { these | these = input }

        Terminal t ->
            Terminal t


demarrer : Model -> Model
demarrer model =
    { model | etape = Definition { definition = "" } }


antonyme : String -> Model -> Model
antonyme def model =
    { model | etape = Antonyme { definition = def, antonyme = "" } }


exemple : { exemple | definition : String, antonyme : String } -> Model -> Model
exemple exemple model =
    { model | etape = Exemple { definition = exemple.definition, antonyme = exemple.antonyme, exemple = "" } }


these : { these | definition : String, antonyme : String, exemple : String } -> Model -> Model
these these model =
    { model | etape = These { definition = these.definition, antonyme = these.antonyme, exemple = these.exemple, these = "" } }



{- VIEW -}


view : Model -> Html.Html Msg
view { etape, problematique } =
    div []
        [ h1 [] [ text problematique ]
        , viewEtape etape
        ]


viewEtape : Etape -> Html.Html Msg
viewEtape etape =
    case etape of
        Initial ->
            button [ onClick Suivant ] [ text "Démarrer" ]

        Definition { definition } ->
            div []
                [ viewInput "Définir la problématique" definition
                , button [ onClick Suivant ] [ text "ok" ]
                ]

        Antonyme { definition, antonyme } ->
            div []
                [ div [styles bordered] [ text definition ]
                , viewInput "Définir l'antonyme" antonyme
                , button [ onClick Suivant ] [ text "ok" ]
                ]

        Exemple exemple ->
            div []
                [ div [] [ text exemple.definition ]
                , div [] [ text exemple.antonyme ]
                , viewInput "Donner un exemple" exemple.exemple
                , button [ onClick Suivant ] [ text "ok" ]
                ]

        These these ->
            div []
                [ div [] [ text these.definition ]
                , div [] [ text these.antonyme ]
                , div [] [ text these.exemple ]
                , viewInput "formuler la thèse" these.these
                , button [ onClick Suivant ] [ text "ok" ]
                ]

        Terminal these ->
            div []
                [ div [] [ text these.definition ]
                , div [] [ text these.antonyme ]
                , div [] [ text these.exemple ]
                , div [] [ text these.these ]
                , button [] [ text "Publier" ]
                ]


viewInput : String -> String -> Html.Html Msg
viewInput ph val =
    textarea
        [ rows 5
        , placeholder ph
        , onInput Defini
        , value val
        ]
        []
