module SpinoCss exposing (styles, bordered)

import Html.Attributes exposing (style)
import Css exposing (border3, px, solid, rgb)

styles = Css.asPairs >> style

bordered = [ border3 (px 1) solid (rgb 11 14 17) ] 
