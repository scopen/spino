# org-mode

| Key binding | Description |
| ----------- | ----------- |
| C-RET     | Add one item (not recognized in terminal/tmux) |
| SPC m RET | Add one item (works in terminal/tmux) |
| t         | cycle TODO tag |


# Requested shorcuts

Please list here the needed shorcuts to be added to this list
